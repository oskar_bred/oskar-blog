<?php

$config = require_once("config.php");

if(isset($_SESSION["auth"]))
    header("Location: index.php");

if(isset($_POST["username"]) && isset($_POST["password"])) {
    if($config["username"] === $_POST["username"] && $config["password"] === $_POST["password"]) {
        $_SESSION["auth"] = true;
        header("Location: index.php");
    } else {
        $error = true;
    }
}


require_once("tpl/login.php");