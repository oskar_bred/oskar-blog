<?php
$config = require_once("config.php");
if (isset($_SESSION["auth"])) {
    unset($_SESSION["auth"]);
    header("Location: index.php");
}
