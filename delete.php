<?php

$filename = "db/posts.json";

$tmpFile = tempnam("db", 'db_');
$tF = fopen($tmpFile, "w");
$f = fopen($filename, "r+");

if (isset($_GET['id']) && file_exists($filename)) {

    $id = $_GET['id'];
    $postFile = "db/" . $id . ".json";

    if(file_exists($postFile)) {

        while (!feof($f)) {
            $str = fgets($f);
            if (strpos($str, $id)) {
                $line = json_decode($str, true);
                if($line['id'] == $id) {
                    continue;
                }
            }

            fwrite($tF, $str);
        }

        fclose($tF);
        fclose($f);
        unlink($postFile);
        rename($tmpFile, $filename);
        chmod($filename, 0777);
    }

}
header("Location: index.php");