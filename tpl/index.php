<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <title><?php echo $config['nameBlog']; ?></title>
</head>
<body>
    <h1 class="main-title"><?php echo $config['nameBlog']; ?></h1>
    <div class="container-fluid">
        <div class="col-md-3">
            <?php
                require_once("menu.php");
            ?>
        </div>
        <div class="col-md-9 blog-body">
            
            <?php foreach($arrPosts as $item) { ?>
            <div class="post">
                <h2 class="post-title"><?php echo $item['title']?></h2>
                <h3 class="post-subtitle"><?php echo $item['summary'] ?>
                </h3>
                
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a><?php echo " " . $item['data'] ?>
                    <a href="<?php echo "post.php?id=".$item['id'] ?>" class="btn btn-primary btn-sm pull-right">Read More</a>
                </p>
                
                <hr />
            </div>
            
              <?php } ?>  
            
            
            <ul class="pagination pull-right" boundary-links="true">
              
                <li class="ng-scope <?php 
				if($page == 1 ){
					echo "disabled";
				}
					?>"><a href="<?php 
				if($page > 1 ){
					$prevPage=$page-1;
				echo "?page=".$prevPage;
				} ?>" class="ng-binding">Previous</a></li>
                
                <?php
$pages=ceil($linecount / $pageSize);
for($i = $page - 3; $i < $page + 3; $i++) {
if($i >= 1 &&  $i <= $pages ){
	$class="";
	if($i==$page){
		$class="active";
	}
echo "<li class=\"$class\"><a href=\"index.php?page=$i\" >$i</a></li>";
}
} ?>
                
                <li class="ng-scope <?php 
				if($page == $pages ){
					echo "disabled";
				}
					?>"><a href="<?php 
				if($page < $pages ){
					$nextPage=$page+1;
				echo "?page=".$nextPage;
				} ?>"
				>Next</a></li>
              
            </ul>
        </div>    
        
    </div>
</body>
</html>