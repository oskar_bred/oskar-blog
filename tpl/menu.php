<?php
if(isset($_POST['town']) && !empty($_POST['town'])) {
$period=null;
if(isset($_POST['period']) && !empty($_POST['period'])) {
    $period="&cnt=".$_POST['period'];
}
    $ch= curl_init("http://api.openweathermap.org/data/2.5/weather?q=".$_POST['town']."&mode=html".$period);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
    $weather=curl_exec($ch);
    $_SESSION["weather"]=$weather;
}
if(!empty($_SESSION["weather"])) {
echo $_SESSION["weather"];
}
?>

<form method="POST">
    <select name="town">
        <option></option>
        <option>Kharkiv</option>
        <option>Poltava</option>
        <option>Kiev</option>
    </select>
	<select name="period">
        <option></option>
		<?php for($i=1;$i<=16;$i++){ ?>
        <option><?php echo $i; ?></option>
        <?php } ?>
    </select>
    <input type="submit" />
</form>

<ul class="nav nav-pills nav-stacked">
    <li>
        <a href="index.php">Main</a>
    </li>
    <?php if(!isset($_SESSION["auth"])) { ?>
    <li>
        <a href="login.php">Login</a>
    </li>
    <?php } else  {
        if (mb_substr($_SERVER['SCRIPT_NAME'], mb_strlen($_SERVER['SCRIPT_NAME']) - 10) !== "create.php") {?>
    <li>
        <a href="create.php">Create Post</a>
    </li>
        <?php } ?>
		
	 <li>
        <a href="create_posts.php">Create few Posts</a>
    </li>	
	 <li> 
        <a href="date.php">Date page</a>
    </li>	
	 <li> 
        <a href="vkimport.php">VK import posts</a>
    </li>	
		
       <?php if (isset($_SESSION["auth"])) {?>
    <li>
        <a href="logout.php">Logout</a>
    </li>
    <?php } }?>
</ul>