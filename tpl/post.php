<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <title><?php echo $config['nameBlog']; ?></title>
</head>
<body>
    <h1 class="main-title"><?php echo $config['nameBlog']; ?></h1>
    <div class="container-fluid">
        <div class="col-md-3">
            <?php
                require_once("menu.php");
            ?>
        </div>
        <div class="col-md-9 blog-body">
        
            <div class="post">

                <h2 class="post-title"><?php echo $post['title'] ?></h2>
                <h3 class="post-subtitle">
                    <?php echo $post['body'] ?>
                </h3>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a><?php echo $post['data'] ?>
                    <?php if (isset($_SESSION['auth'])) { ?>
					 <a href="<?php echo "edit.php?id=" . $_GET['id'] ?>" class="btn btn-primary btn-sm pull-right">Edit post</a>
                    <a href="<?php echo "delete.php?id=" . $_GET['id'] ?>" class="btn btn-primary btn-sm pull-right">Delete post</a>
                    <?php }?>
                </p>
                
                <hr />
            </div>
            
        </div>    
        
    </div>
</body>
</html>