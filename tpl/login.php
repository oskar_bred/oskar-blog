<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <title><?php echo $config['nameBlog']; ?></title>
</head>
<body>
    <h1 class="main-title"><?php echo $config['nameBlog']; ?></h1>
    <div class="container-fluid">
        <div class="col-md-3">
            <?php
                require_once("menu.php");
            ?>
        </div>
        <div class="col-md-9 blog-body">
        
            <div class="col-lg-6 col-lg-offset-3 ng-scope">
            
            
                <div class="panel panel-success" style="margin-top:20px;">
                    <div class="panel-heading">
                        <h2 style="margin:0;" class="ng-binding">Login</h2>
                    </div>
                    <div class="panel-body">
                
                        
                            <form method="POST" action="" ng-submit="loginForm.submit()" novalidate="" name="loginFrm" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required">
                                <?php if(isset($error))
                                        echo '<div class="alert alert-danger">Auth Error!</div>';
                                if(isset($vkError))
                                    echo '<div class="alert alert-danger">'.$vkError.'</div>';
                                      if(isset($_SESSION["auth"]))
                                        '<div class="alert alert-success">Auth Success!</div>';
                                ?>
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-email">Username</label>
                                    <input type="text" id="loginform-username" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required" name="username" ng-model="loginForm.email" ng-required="true" required="required">
                                </div>
                    
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-password">Password</label>
                                    <input type="password" id="loginform-password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required" name="password" ng-model="loginForm.password" ng-required="true" required="required">
                                </div>
                    
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="remember-me" name="rememberMe" ng-model="loginForm.rememberMe" class="ng-pristine ng-untouched ng-valid"> Keep me logged in
                                    </label>
                                </div>
                    
                                <div class="form-group">
                                    <input type="submit" id="login-button" class="btn btn-primary form-control" value="Login">
                                </div>
                
                            </form>
                        <div class="form-group">
                            <a href="https://oauth.vk.com/authorize?client_id=5063810&display=page&scope=friends,video,wall,offline &redirect_uri=http://localhost/blog/vklogin.php&response_type=code&v=5.37" id="login-button" class="btn btn-primary form-control" > Войти с помощью Вконтакте </a>
                        </div>
                
                    </div>
                </div>
            </div>
        </div>    
        
    </div>
</body>
</html>