<?php
$config = require_once("config.php");
$filejson = "db/posts.json";
if(isset($_SESSION["auth"])) {
    require_once("tpl/create.php");
    if(isset($_POST["summary"]) && isset($_POST["title"]) && isset($_POST["body"])) {
        $id = sha1(time() . mt_rand(0, 10000)); 
        $arr = array(
            "id" => $id,
            "title" => $_POST["title"],
            "summary" => $_POST["summary"],
            "data" => date("d.m.Y H:i:s")
        );
        file_put_contents($filejson, json_encode($arr) . "\n", FILE_APPEND);
        $arr["body"] = $_POST["body"];
        //var_dump(json_encode($arr));
        file_put_contents("db/" . $id . ".json", json_encode($arr));
    }
} else {
    header("Location: index.php");
}